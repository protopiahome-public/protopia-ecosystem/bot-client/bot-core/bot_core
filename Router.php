<?php


class Router
{
    private static $routes = array();

    private function __construct() {}
    private function __clone() {}

    public static function route($pattern, $callback)
    {
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
//        $pattern = '/^' . preg_quote($pattern, '/') . '$/';
//        Logger::log_msg("pattern: ".$pattern);
        self::$routes[$pattern] = $callback;
    }


    public static function redirect($path1, $path2){
        self::$routes[$path1] = self::$routes[$path2];
    }

    public static function goTo($position){
        $_SESSION["HISTORY"][] = $position;
    }

    public static function goBack(){
        array_pop($_SESSION["HISTORY"]);
    }

    public static function goBegin(){
        array_shift($_SESSION["HISTORY"]);
    }

    public static function current($data){
        if($_SESSION["HISTORY"]){
            $position =  end($_SESSION["HISTORY"]);
        }else{
            $position = "/";
            $_SESSION["HISTORY"][] = $position;
        }
        Logger::log_msg("position: ".$position);
        Router::execute($position, $data);
    }

    public static function push($url, $data){
        $_SESSION["HISTORY"][] = $url;
        end($_SESSION["HISTORY"]);
        Router::execute($url, $data);
    }

    public static function home($url, $data){

        unset($_SESSION["HISTORY"]);
        $_SESSION["HISTORY"][] = $url;
        Router::execute($url, $data);
    }

    public static function execute($url, $data)
    {
        foreach (self::$routes as $pattern => $callback)
        {
            if (preg_match($pattern, $url, $params))
            {
                array_shift($params);
                $url_params = array_values($params);
                return call_user_func_array($callback, array($data, $url_params));
            }
        }
    }

}
